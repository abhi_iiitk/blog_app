import pandas as pd
import numpy as np
from node import Node
data = pd.read_csv('Rules.csv')
raw_rules = list(data["Patterns"])
#print(raw_rules)
cleaned_rules = []
for word in raw_rules:
  a = list(word)
  n = len(a)
  #print(n)
  b = []
  for i in range(n):
    #print(i)
    #print(a[i])
    if a[i].isalpha() or a[i].isdigit() or a[i].isspace() or a[i] == '+':
      b.append(a[i])
  c = "".join(b)
  cleaned_rules.append(c)

#n = Node("analyse")
#n.children.update({"ram": Node("ram1")})
#print(n.children.keys())
#print(cleaned_rules)

tree = {}

for word in cleaned_rules:
  st = word.split(" ")
  #print(st)
  rule_size = len(st)
  key = st[0]
  root = Node(st[0])
  if not(key in tree):
    tree[key]=root

  if(rule_size > 1):
    for i in range(rule_size):
      child = st[i]
      if(i != 0):
        if not(child == 'AND' or child == 'OR' or child == '+'):
          tree[key].children[child] = Node(child)
          #print(" " + str(key) + ":" + str(child))
          #child_dict.update({child: Node(child)})
  #print(str(tree[key].value)+ ":"+ str(tree[key].children))

li = list(tree.keys())
print(len(li))
for i in li:
  print("childrens of " + str(tree[i].value) + ":-" + str(list(tree[i].children.keys())))


